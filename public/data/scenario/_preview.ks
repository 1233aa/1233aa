[_tb_system_call storage=system/_preview.ks ]

[mask time=10]
[bg  time="10"  method="crossfade"  storage="rouka.jpg"  ]
[playbgm  volume="100"  time="1000"  loop="true"  storage="菜鸟图库-轻音乐欢快背.mp3"  ]
[tb_show_message_window] 
[mask_off time=10]
[chara_show  name="ゆうごう"  time="1000"  wait="true"  storage="chara/2/ef3b27f1991502bddddbaa07cbd7d14.png"  width="632"  height="799"  left="-11"  top="140"  reflect="false"  ]
[tb_start_text mode=1 ]
あなたは彼に片思いしているので、あなたはどのようにします…[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="ご挨拶しない"  x="887"  y="497"  width=""  height=""  _clickable_img=""  target="*no"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  x="885"  y="411"  width=""  height=""  text="ご挨拶します"  _clickable_img=""  target="*yes"  ]
[s  ]
*no

[chara_hide  name="ゆうごう"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="room.jpg"  ]
[tb_start_text mode=1 ]
ナンパするのが怖かったんです[p]
突然！[p]
夢の中で目が覚める[p]
[_tb_end_text]

[chara_show  name="小林"  time="1000"  wait="true"  storage="chara/1/komaru.png"  width="400"  height="533"  ]
[tb_start_text mode=1 ]
#小林
夢だったんですね。。。[p]
[_tb_end_text]

[chara_hide  name="小林"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="title.jpg"  ]
[tb_start_text mode=1 ]
#
遊びありがとうございます[p]
[_tb_end_text]

[s  ]
[jump  storage="scene1.ks"  target="*common"  ]
*common

*yes

[chara_show  name="小林"  time="1000"  wait="true"  storage="chara/1/77202da7d74948a7b1f91650aa58aa6.png"  width="482"  height="609"  left="753"  top="119"  reflect="true"  ]
[tb_start_text mode=1 ]
#小林
ゆうごうさんじゃないですか[p]
ゆうごうさんどうしてまだ家に帰らないのですか。[p]
今日はバスケの試合がありますか[p]
[_tb_end_text]

[chara_mod  name="ゆうごう"  time="600"  cross="true"  storage="chara/2/dbb1b051aad507e97d6554bc2436cfe.png"  ]
[tb_start_text mode=1 ]
#ゆうごう
えーー[p]
あなたは誰ですか?[p]
あなたに会ったことがないようです[p]

[_tb_end_text]

[tb_start_text mode=1 ]
#小林
隣のクラスの,もう知らないの？小林です[p]
[_tb_end_text]

[chara_mod  name="ゆうごう"  time="600"  cross="true"  storage="chara/2/96aa575e698b64a9bcb3d106f8c5ce6.png"  ]
[tb_start_text mode=1 ]
#ゆうごう
でも私の知っている小林さんはそんな顔じゃありません。。[p]
[_tb_end_text]

[chara_mod  name="小林"  time="600"  cross="true"  storage="chara/1/24b5d06c9a91a7c12a939a235c38851.png"  ]
[tb_start_text mode=1 ]
#小林
え？！？！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#
急いでトイレに行って鏡を見てみると、[p]
元とは変わっています。[p]
それで[p]
あなたは小林のクラスメートのふりをしてゆうごうに彼の好きなことを言うことにしました[p]
[_tb_end_text]

[chara_mod  name="小林"  time="600"  cross="true"  storage="chara/1/77202da7d74948a7b1f91650aa58aa6.png"  ]
[tb_start_text mode=1 ]
#小林
はははははは、嘘ですよ、僕は小林さんのクラスメイトで、小林さんはあなたのことが大好きです[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#ゆうごう
本当ですか?私も小林さんのことがとても好きでしたが、[p]
小林さんに告白したくなかったのです。[p]
断られるのが怖かったからです。[p]
[_tb_end_text]

[chara_hide  name="小林"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="ゆうごう"  time="1000"  wait="true"  pos_mode="true"  ]
[tb_start_text mode=1 ]
突然下校のベルの音が聞こえました、、[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="room.jpg"  ]
[chara_show  name="小林"  time="1000"  wait="true"  storage="chara/1/komaru.png"  width="400"  height="533"  ]
[tb_start_text mode=1 ]
#小林
もとは1回の夢で、[p]
同じく本当に私が好きかどうかを知りません…[p]
[_tb_end_text]

[quake  time="300"  count="3"  hmax="10"  wait="true"  ]
[chara_show  name="ゆうごう"  time="1000"  wait="true"  storage="chara/2/84ae3b0cace681596af9dbfa6a428ec.png"  width="516"  height="653"  left="826"  top="69"  reflect="false"  ]
[tb_start_text mode=1 ]
#ゆうごう
やっと見つけました! ! [p]
小林さん！！[p]
あなたにお伝えしたいことがあります[p]

[_tb_end_text]

[chara_mod  name="小林"  time="600"  cross="true"  storage="chara/1/ikari.png"  ]
[tb_start_text mode=1 ]
#小林
何があったんですか?[p]
なんか焦ってますね。[p]
[_tb_end_text]

[chara_mod  name="ゆうごう"  time="600"  cross="true"  storage="chara/2/96aa575e698b64a9bcb3d106f8c5ce6.png"  ]
[tb_start_text mode=1 ]
#ゆうごう
好きです,小林さん,告白を受け入れてください。[p]
[_tb_end_text]

[chara_mod  name="小林"  time="600"  cross="true"  storage="chara/1/komaru.png"  ]
[tb_start_text mode=1 ]
#小林
 え？！？！[p]
それは本当ですか?[p]
本当に告白されるなんて信じられません。[p]
[_tb_end_text]

[chara_mod  name="ゆうごう"  time="600"  cross="true"  storage="chara/2/84ae3b0cace681596af9dbfa6a428ec.png"  ]
[tb_start_text mode=1 ]
#ゆうごう
もちろん本当で、[p]
私は本当にとてもあなたが好きで、[p]
毎日授業が終わって私はすべてあなたの一挙一動に注目して、[p]
私は本当にとてもあなたが好きで、私の告白を受け入れてください。[p]
[_tb_end_text]

[chara_mod  name="小林"  time="600"  cross="true"  storage="chara/1/nyaa.png"  ]
[tb_start_text mode=1 ]
#小林
やった、やっと好きな人と一緒に来ました![p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  target="*つつく"  text="つつく"  x="972"  y="629"  width=""  height=""  _clickable_img=""  ]
*つつく

[mask_off  time="1000"  effect="fadeOut"  ]
[s  ]
